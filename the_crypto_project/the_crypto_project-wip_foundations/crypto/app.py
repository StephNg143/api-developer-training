from time import sleep

# This class defines the app engine.
class App:

    # The app engine is connected to a list of events.
    def __init__(self, events, time_interval):
        self.events = events
        self.time_interval = time_interval

    # For now, when starting the application, each event
    # is checked only once.
    def start(self):
        while True:
            for event in self.events:
                event.check()

            sleep(self.time_interval)
